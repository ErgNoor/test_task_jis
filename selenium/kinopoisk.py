# coding: utf-8

from selenium.webdriver.chrome import webdriver

# для селениума так же пишутся тесты в отдельных файлах?

driver = webdriver.WebDriver()
driver.get("https://www.kinopoisk.ru")

query_input = driver.find_element_by_xpath(".//input[@name='kp_query' and @class='header-search-partial-component__search-field']")
query_input.send_keys("hua mu lan")

find_button = driver.find_element_by_xpath(".//input[@class='header-search-partial-component__button']")
find_button.click()

driver.quit()

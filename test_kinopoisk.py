# coding: utf-8

from kinopoisk import get_page, get_slogan
from lxml import html, etree
from requests.exceptions import MissingSchema
import pytest


@pytest.fixture
def page():
    """Returns the html page for scraping"""
    return get_page("https://www.kinopoisk.ru/film/ip-man-2-2010-450753/)")


class TestGetPage():
    def test_raises_exception_for_lack_of_argument(self):
        with pytest.raises(MissingSchema):
            get_page()

    def test_raises_exception_on_non_string_argument(self):
        with pytest.raises(MissingSchema):
            get_page(42)

    def test_string_argument_is_empty(self):
        with pytest.raises(MissingSchema):
            get_page("")

    def test_url_param(self, page):
        assert(isinstance(page, str))

    def test_page_is_correct(self, page):
        # smoke
        # if page is correct then title is "film_name(year) - some_text - КиноПоиск"
        # if page is not correct(too many requests->captcha, for example) then title is "Кинопоиск.ру"

        # если smoke один, то как правильно его вынести из класса в отдельный модуль?(копия класса, но со смок тестом/тестами?)
        doc_page = html.fromstring(page)
        assert(doc_page.xpath(".//title")[0].text.lower() != u"кинопоиск.ру")


class TestSlogan():
    # как правильно организовать и замокать?

    def test_slogan_is_str(self):
        assert(isinstance(get_slogan(html.fromstring(page)), str))

# coding: utf-8

import requests
from lxml import html, etree

# url = "https://www.kinopoisk.ru/film/khua-mulan-1964-116327/"
url_1 = "https://www.kinopoisk.ru/film/ip-man-2-2010-450753/"


def get_page_from_file():
    # tree = etree.parse("hua_mu_lan.htm", etree.HTMLParser())
    tree = etree.parse("ip_man_2.htm", etree.HTMLParser())
    return tree


def get_page(url=None):
    # if not url or not isinstance(url, str):
    #     print("url should be not empty string")
    #     return
    return requests.get(url).text.encode("utf-8")


def parse(page):
    # doc_page = html.fromstring(page)  # something goes wrong here...
    doc_page = page

    film_name = get_film_name(doc_page)
    slogan = get_slogan(doc_page)
    reviews = get_reviews(doc_page)

    return film_name, slogan, reviews


def get_reviews(doc_page):
    stat_reviews = {}

    good_review_xpath = ".//div[contains(@class, 'response') and contains(@class, 'good') and contains(@itemprop, 'reviews')]"
    bad_review_xpath = ".//div[contains(@class, 'response') and contains(@class, 'bad') and contains(@itemprop, 'reviews')]"
    review_xpath = ".//div[contains(@class, 'response') and contains(@itemprop, 'reviews')]"

    reviews = doc_page.xpath(review_xpath)
    if not reviews:
        num_reviews = num_bad_reviews = num_good_reviews = num_neutral_reviews = 0
    else:
        num_reviews = len(reviews)
        num_bad_reviews = len(doc_page.xpath(bad_review_xpath))
        num_good_reviews = len(doc_page.xpath(good_review_xpath))
        num_neutral_reviews = num_reviews - (num_bad_reviews + num_bad_reviews)
    stat_reviews["num"] = num_reviews
    stat_reviews["bad"] = num_bad_reviews
    stat_reviews["good"] = num_good_reviews
    stat_reviews["neutral"] = num_neutral_reviews

    return stat_reviews


def get_slogan(doc_page):
    slogan_xpath = u".//td[contains(text(), 'слоган')]/../td[2]"
    slogan = doc_page.xpath(slogan_xpath)[0].text

    return slogan


def get_film_name(doc_page):
    film_name = {}

    name_xpath_rus = ".//h1[@class='moviename-big']"
    name_xpath_orig = name_xpath_rus + "/../span"
    film_name["rus"] = doc_page.xpath(name_xpath_rus)[0].text
    film_name["orig"] = doc_page.xpath(name_xpath_orig)[0].text

    return film_name


# page = get_page(url_1)
page = get_page_from_file()
parsed_data = parse(page)
# print(parsed_data)
